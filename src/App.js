import React, { Component } from 'react';
import Usuario from './components/Usuario';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      users: []
    };
  }

  componentDidMount() {
    this.loadUsuarios();
  }

  async loadUsuarios() {
    const res = await fetch('https://randomuser.me/api/?results=20');
    const json = await res.json();

    /**
     * Alguns registros não
     * possuem id
     */
    let customId = 1;

    const users = json.results.map(user => {
      const id = user.id.value || (customId++).toString();

      /**
       * Desestruturando chaves
       */
      const { name, picture, email, dob } = user;

      return {
        id,
        name: name.title + ' ' + name.first + ' ' + name.last,
        avatar: picture.large,
        email: email,
        idade: dob.age
      };
    });

    this.setState({
      users
    });
  }

  render() {
    /**
     * Desestruturando state
     */
    const { users } = this.state;

    return (
      <div>
        <h4>Listando 20 usuários:</h4>
        {users.map(user => {
          /**
           * Desestruturando chaves
           */
          const { id, name, avatar, email, idade } = user;

          return (
            <Usuario
              key={id}
              nome={name}
              avatar={avatar}
              email={email}
              idade={idade}
            />
          );
        })}
      </div>
    );
  }
}
