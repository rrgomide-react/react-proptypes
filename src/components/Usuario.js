import React from 'react';
import PropTypes from 'prop-types';

import './usuario.css';
import noAvatar from '../assets/no-avatar.png';

const Usuario = props => {
  return (
    <div className="card">
      <div className="data">
        <img className="avatar space" src={props.avatar} alt={props.nome} />
        <div className="list space">
          <span>
            <strong>Nome:</strong> {props.nome}
          </span>
          <span>
            <strong>Email:</strong> {props.email}
          </span>
          <span>
            <strong>Idade:</strong> {props.idade}
          </span>
        </div>
      </div>
    </div>
  );
};

/**
 * Criação de objeto propTypes
 * para a classe Usuaio
 */
Usuario.propTypes = {
  /**
   * Indicando 'nome' como string obrigatória
   */
  nome: PropTypes.string.isRequired,

  /**
   * Indicando 'email' como string obrigatória
   */
  email: PropTypes.string.isRequired,

  /**
   * Indicando 'idade' como number não obrigatório
   */
  idade: PropTypes.number,

  /**
   * Indicando 'avatar' como string
   * não obrigatória
   */
  avatar: PropTypes.string
};

/**
 * Definindo valores padrão
 * para cada prop
 */
Usuario.defaultProps = {
  nome: 'Nenhum nome definido',
  email: 'nenhumemail@informado.com',
  idade: -1,
  avatar: noAvatar
};

export default Usuario;
